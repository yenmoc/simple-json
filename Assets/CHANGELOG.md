# Changelog

## [1.0.1] - 16-01-2020
### Changed
	-Added Clone() method to JSONNode to allow cloning of a whole node tree.
